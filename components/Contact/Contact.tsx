"use client";

import clsx from "clsx";
import SectionTittle from "components/sectionTittle/sectionTittle";
import React, { useState } from "react";
import { CiMail } from "react-icons/ci";
import { FaGithub, FaLinkedin } from "react-icons/fa";
import styled from "styled-components";

type Contact = {
  name: string;
  href: string;
  Icon: React.JSX.Element;
};

const listContact: Contact[] = [
  {
    name: "GitHub",
    href: "https://github.com/SebasUNLu",
    Icon: <FaGithub className="w-full h-full" />
  },
  {
    name: "LinkedIn",
    href: "https://www.linkedin.com/in/sebasti%C3%A1n-pedro-marchetti/",
    Icon: <FaLinkedin className="w-full h-full" />
  },
  {
    name: "Mail",
    href: "mailto:seba.p.marchetti@gmail.com",
    Icon: <CiMail className="w-full h-full" />
  }
];

const DivSocial = styled.a`
  width: 40%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  position: relative;

  .iconDiv {
    width: 50%;
    max-width: 100px;
    display: flex;
  }

  @media (min-width: 640px) {
    .iconDiv {
      width: 100%;
    }
  }
`;

// TODO crear componente para elemento Contact, que tenga, en sm, funcion de mostrar un titulo al pasar el cursor
function ContactElement({ contact }: { contact: Contact }) {
  const { name, href, Icon } = contact;
  const [show, setShow] = useState(false);
  return (
    <DivSocial
      href={href}
      onMouseEnter={() => {
        setShow(true);
      }}
      onMouseLeave={() => {
        setShow(false);
      }}
    >
      <div className="iconDiv">{Icon}</div>
      <div
        className={clsx("absolute flex justify-center top-full left-0 right-0 mx-auto transition-opacity duration-500 ease-in-out", {
          "opacity-100": show,
          "opacity-0": !show
        })}
      >
        <p className="text-center px-4 py-2 mt-4 bg-black-800 rounded-lg">{name}</p>
      </div>
    </DivSocial>
  );
}

function Contact() {
  return (
    <section id="contact" className="w-full mb-16">
      <SectionTittle tittle="Contacto" className="mb-8" />
      <div className="w-full flex flex-wrap sm:flex-row sm:flex-nowrap items-center justify-center sm:justify-around gap-8">
        {listContact.map((contact) => (
          <ContactElement contact={contact} key={`contact_${contact.name}`} />
        ))}
      </div>
    </section>
  );
}

export default Contact;
