import SectionTittle from "components/sectionTittle/sectionTittle";
import React from "react";

type Experience = {
  tittle: String;
  duration: String;
  responsabilities: String[];
};

const experiences: Experience[] = [
  {
    tittle: "Pasante en la Universidad Nacional de Lujan",
    duration: "Marzo de 2024 - Actualidad",
    responsabilities: [
      "Participación de Pasantía Interna Rentada (PIR).",
      "Participación en actividdades extracurriculares.",
      "Mantenimiento de una página web proveída por la institución."
    ]
  }
];

function Experience() {
  return (
    <section className="w-full p-4 flex flex-col gap-4">
      <SectionTittle tittle="Experiencia" />
      <div className="w-full flex items-center justify-center">
        {experiences.map((exp, index) => (
          <div className="w-full sm:w-10/12 p-4" key={`${exp.tittle}_${index}`}>
            <p className="text-2xl sm:text-3xl">{exp.tittle}</p>
            <p className="text-sm">{exp.duration}</p>
            <ul className="list-disc list-inside">
              {exp.responsabilities.map((resp) => (
                <li className="" key={`${resp}`}>{resp}</li>
              ))}
            </ul>
          </div>
        ))}
      </div>
    </section>
  );
}

export default Experience;
