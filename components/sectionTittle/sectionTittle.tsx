import clsx from "clsx";
import React from "react";

type TittleProps = {
  tittle: string | React.ReactNode;
  className?: string;
}

const SectionTittle = ({ tittle, className = "" }: TittleProps) => {
  return <h2 className={clsx("w-full text-5xl text-center font-bold", className)}>{tittle}</h2>;
};

export default SectionTittle;
