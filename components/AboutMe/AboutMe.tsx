import SectionTittle from "components/sectionTittle/sectionTittle";
import Image from "next/image";
import React from "react";

function AboutMe() {
  return (
    <section id="about" className="bg-transparent w-full flex flex-col p-8 gap-8">
      <SectionTittle tittle="About Me" />
      <div className="flex flex-col gap-8 sm:flex-row">
        {/* Text 1 */}
        <div className="flex flex-col gap-4 w-full">
          <p className="">
            Empecé estudiando esta carrera por curiosidad sobre el mundo de la programación. Durante los años que estuve
            cursando pude ver que no era tan directo como pensaba, este mundo tiene un montón de ramas de aprendizaje.
            Por un tiempo me sentí asombrado y perdido, hasta que descubrí que me interesaba más el desarrollo Web. Opte
            por dejar en pausa la universidad para perseguir este ideal. Uno de mis más grandes amigos me recomendó el
            bootcamp SoyHenry. En él, pude empezar a adentrarme más y más en este mundo, y conocer a maravillosas
            personas, con los mismos ideales (y humor) que yo.
          </p>
          <p className="">
            En la universidad estuve trabajando principalmente con Java para proyectos importantes, además de usar
            Assembler y experimentar con arduinos. Vimos los fundamentos principales de las bases de datos relacionales
            usando SQL y Firebird, con uno de los profesores más estríctos. Finalmente, cuando empecé a ver la
            programación web y javascript, sentí que me desenvolvía con facilidad, y luego de empezar a trabajar con
            React supe que este era el ambiente en el que me adaptaría mejor.
          </p>
        </div>
        {/* Images */}
        <div className="flex flex-col gap-4 w-full justify-around">
          <Image src="/assets/img/about_univ.webp" width={680} height={365} alt="university_image" className="w-full" />
          <Image
            src="/assets/img/about_henry.webp"
            width={1900}
            height={1080}
            alt="university_image"
            className="w-full"
          />
        </div>
      </div>
      <div className="">
        <p className="">
          Ahora mismo me encuentro desarrollando con Next.js e implementando Typescript, y sigo buscando la oportunidad
          de poner en práctica todos mis conocimientos.
        </p>
      </div>
    </section>
  );
}

export default AboutMe;
