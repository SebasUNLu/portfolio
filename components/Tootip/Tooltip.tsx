"use client";
import { clsx } from "clsx";
import React, { useEffect } from "react";
type Position = "top" | "bottom" | "left" | "right";
interface TooltipProps {
  tootipId?: string;
  className?: string;
  spacing?: number;
  position?: Position;
  isShowTootip?: boolean;
}

function Tooltip(props: TooltipProps) {
  const { className = "", spacing = 8, position = "top", isShowTootip } = props;
  const [unMount, setUnMount] = React.useState(false);

  useEffect(() => {
    return () => {
      setUnMount(true);
    };
  }, []);

  useEffect(() => {
    if (unMount) return;
    const tooltipTriggers = document.querySelectorAll("[data-tooltip-target]");
    const target = document.getElementById("tooltip");

    tooltipTriggers.forEach((trigger, index) => {
      const handleShowTooltip = () => {
        if (!target) {
          return;
        }
        console.log(index);
        const triggerRect = trigger.getBoundingClientRect();
        const width = Math.floor(triggerRect?.width || 0);
        const height = Math.floor(triggerRect?.height || 0);

        const tooltip = target?.getBoundingClientRect();
        const tooltipWidth = Math.floor(tooltip?.width || 0);
        const tooltipHeight = Math.floor(tooltip?.height || 0);

        const positionTooltips = {
          bottom: {
            top: `${triggerRect.bottom + spacing}px`,
            left: `${triggerRect.left}px`
          },
          top: {
            top: `${triggerRect.top - tooltipHeight - spacing}px`,
            left: `${triggerRect.left}px`
          },
          left: {
            top: `${triggerRect.top + (height / 2 - tooltipHeight / 2)}px`,
            left: `${triggerRect.left - tooltipWidth - spacing}px`
          },
          right: {
            top: `${triggerRect.top + (height / 2 - tooltipHeight / 2)}px`,
            left: `${triggerRect.right + spacing}px`
          }
        };

        target.style.width = `${width}px`;
        target.style.top = positionTooltips[position].top;
        target.style.left = positionTooltips[position].left;
        target.classList.remove("invisible");
        target.classList.add("visible");
      };

      const handleHideTooltip = () => {
        if (!target) {
          return;
        }
        target.classList.remove("visible");
        target.classList.add("invisible");
      };

      trigger.addEventListener("mouseenter", handleShowTooltip);
      trigger.addEventListener("mouseleave", handleHideTooltip);
    });
  }, [unMount]);

  return (
    <div
      id="tooltip"
      className={clsx(
        isShowTootip ? "visible" : "invisible",
        "z-50 fixed flex items-center h-max invisible break-all px-3 py-2 text-sm font-medium text-white transition-opacity duration-300 bg-gray-400 rounded-md shadow-sm ",
        className
      )}
    >
      tootltipsrgregregregrejjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj
      <div className="absolute w-2 h-2 -z-50 bg-gray-400 transform rotate-45 top-[calc(100%_-_4px)] left-[calc(50%_-_4px)]" />
    </div>
  );
}

export default Tooltip;
