import React from 'react'

function Footer() {
  return (
    <footer className='w-full h-16 p-4 flex items-center justify-center bg-black-800 text-grey-500'>© 2024 Portafolio de Seba</footer>
  )
}

export default Footer