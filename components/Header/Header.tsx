"use client";

import { useState } from "react";
import { Bars3Icon, XMarkIcon } from "@heroicons/react/24/solid";
import clsx from "clsx";

const navlinks = [
  { name: "Home", href: "#home" },
  { name: "About", href: "#about" },
  { name: "Skills", href: "#skills" },
  { name: "Projects", href: "#projects" },
  { name: "Contact", href: "#contact" },
  { name: "Experience", href: "#experience" }
];

function Header() {
  const [menuOpen, setMenuOpen] = useState(false);

  const openCloseSide = () => {
    setMenuOpen(!menuOpen);
  };

  return (
    <>
      <header className="z-10 bg-black-800 h-16 px-6 py-4 flex items-center justify-between fixed top-0 left-0 w-full">
        <h1 className="text-2xl font-bold">Portfolio</h1>
        <nav className="gap-4 w-full justify-center hidden sm:flex">
          {navlinks.map((link) => (
            <a href={link.href} key={link.name} className="p-1">
              {link.name}
            </a>
          ))}
        </nav>
        <Bars3Icon className="h-full cursor-pointer sm:hidden" onClick={openCloseSide} />
      </header>

      {/* Side bar, only visible within sm --> 640px */}
      {/* black fade */}
      <div
        className={clsx("z-20 fixed w-full h-full top-0 left-0 bg-black-500 opacity-50 transition-all duration-75", {
          "translate-x-full": !menuOpen
        })}
        onClick={openCloseSide}
      ></div>
      {/* menu */}
      <div
        className={clsx("z-30 fixed w-1/2 h-full top-0 right-0 bg-black-500 flex flex-col gap-4 transition-all duration-75", {
          "translate-x-full": !menuOpen
        })}
      >
        <div className="h-16 flex justify-end items-center px-6" onClick={openCloseSide}>
          <XMarkIcon className="sm:hidden h-1/2"/>
        </div>
        {navlinks.map((link) => (
          <a href={link.href} key={link.name} className="px-4 py-2 w-full font-bold " onClick={openCloseSide}>
            {link.name}
          </a>
        ))}
      </div>
    </>
  );
}

export default Header;
