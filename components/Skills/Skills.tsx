import React from "react";
import skills from "./skillList";
import SectionTittle from "components/sectionTittle/sectionTittle";
import { clsx } from "clsx";

function Skills() {
  return (
    <section id="skills" className="w-full p-4">
      <SectionTittle tittle="Skills" className="mb-4" />
      <div className="w-full flex flex-wrap items-center justify-center gap-y-16 text-xs sm:text-base">
        {skills.map(({ Icon, name, classname = "" }, index) => (
          <div key={`skill_${index}`} className={clsx("p-2 rounded text-center w-2/5 sm:w-1/3 transition-all duration-200", classname)}>
            <Icon className="w-full h-[100px]" />
            <p className="w-full font-bold">{name}</p>
          </div>
        ))}
      </div>
    </section>
  );
}

export default Skills;
