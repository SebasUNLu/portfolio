import { IconType } from "react-icons";
import { IoLogoJavascript } from 'react-icons/io5';
import { FaReact } from "react-icons/fa";
import { FaNodeJs } from "react-icons/fa";
import { SiTypescript } from "react-icons/si";

type Skill = {
  name: string,
  Icon: IconType,
  classname?: string
}

const skills: Skill[] = [
  { name: "Javascript", Icon: IoLogoJavascript, classname: "hover:text-yellow-400" },
  { name: "React", Icon: FaReact, classname:"hover:text-blue-500" },
  { name: "Node.js", Icon: FaNodeJs, classname: "hover:text-green-500" },
  { name: "Typescript", Icon: SiTypescript, classname: "hover:text-blue-800" },
  { name: "Next 13, 14", Icon: FaReact, classname:"hover:text-blue-500" },
];


export default skills