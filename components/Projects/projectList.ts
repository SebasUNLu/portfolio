type Project = {
  tittle: string;
  description: string;
  href: string;
}

const projectList: Project[] = [
  {
    tittle: "Henry Shop",
    description: "E-commerce de productos con temática del bootcamp SoyHenry. Desarrollado como proyecto final del bootcamp Henry.",
    href: "https://github.com/SebasUNLu/HenryShop"
  }
];

export default projectList;