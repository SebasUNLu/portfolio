import SectionTittle from "components/sectionTittle/sectionTittle";
import { FaRegFolderOpen } from "react-icons/fa6";
import React from "react";
import projectList from "./projectList";

function Projects() {
  return (
    <section id="projects" className="w-full">
      <SectionTittle tittle="Proyectos" className="mb-8" />
      {/* list */}
      <div className="w-full flex flex-col gap-8 items-center justify-center sm:flex-row sm:flex-wrap">
        {/* individual projects */}
        {projectList.map(({ description, href, tittle }, index) => (
          <a
            className="flex w-3/4 sm:w-2/5 border-white border-[1px] rounded-md p-4 transition-all duration-200 hover:scale-105"
            href={href}
            key={`project_${index}`}
          >
            <div className="flex flex-col w-full">
              <div className="w-full flex justify-between items center">
                <FaRegFolderOpen className="w-auto h-full" />
                <p className="font-bold text-3xl text-center mb-2 w-full">{tittle}</p>
              </div>
              <p className="text-center text-md my-4">{description}</p>
              <p className="text-center text-gray-400 text-sm underline">go to github</p>
            </div>
          </a>
        ))}
      </div>
    </section>
  );
}

export default Projects;
