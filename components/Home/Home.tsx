import React from "react";
import Image from "next/image";
import SectionTittle from "components/sectionTittle/sectionTittle";

function Home() {
  return (
    <div className="w-full flex items-center gap-7 flex-col sm:flex-row-reverse p-4 sm:p-8">
      <Image
        src="/assets/img/profile.jpg"
        alt="profile"
        width={360}
        height={360}
        priority
        className="rounded-[5em] w-1/2"
      />
      <div className="flex flex-col items-center justify-center gap-8">
        <SectionTittle
          tittle={
            <>
              Hi, I'm <span className="text-[#f21414]">Sebastián Pedro Marchetti</span>
            </>
          }
        />
        <p className="text-white text-center">
          Full Stack Developer with training as a Web Designer. Experience working in NodeJS, React, Express, Next.js,
          Typescript, among other technologies in the sector.
        </p>
      </div>
    </div>
  );
}

export default Home;