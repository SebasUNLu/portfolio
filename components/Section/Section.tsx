import React from "react";
import { clsx } from "clsx";

interface SectionProps {
  title: string;
  className?: string;
  children: React.ReactNode;
}

function Section(props: SectionProps) {
  const { className = "", children, title } = props;
  return (
    <section id={`${title}`} className={clsx("w-full max-w-4xl flex justify-center my-4",className)}>
      {children}
    </section>
  );
}

export default Section;
