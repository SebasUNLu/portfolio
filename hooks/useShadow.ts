"use client";
import { useEffect } from "react";

interface ShadowProps {
  distance?: number;
  colorRgb?: string;
}

function useShadow(props: ShadowProps) {
  const { distance = 7, colorRgb = "242, 20, 20" } = props;
  function growthRate(valueInital: number, growthRate: number) {
    return valueInital * Math.pow(growthRate, 1);
  }
  useEffect(() => {
    const elements: NodeListOf<Element> = document.querySelectorAll("[data-shadow]");
    elements.forEach((element: Element) => {
      if (!element.hasAttribute("data-shadow-applied")) {
        element.setAttribute("data-shadow-applied", "true");
        const styles = {
          transition: "box-shadow ease-in-out",
          "border-radius": "5px"
        };
        const htmlElement = element as HTMLElement;
        Object.assign(htmlElement.style, styles);
      }
    });

    document.addEventListener("mousemove", (cursor) => {
      if (!elements) {
        return;
      }
      elements.forEach((element: Element) => {
        const { left, height, width, top } = element?.getBoundingClientRect();
        const defaultBlur = 4;
        const defaultOpacity = 1;
        const defaultSize = 3;

        const elementCenterX = left + width / 2;
        const elementCenterY = top + height / 2;
        const cursorValueX = cursor.clientX;
        const cursorValueY = cursor.clientY;
        const distanceX = elementCenterX - cursorValueX;
        const distanceY = elementCenterY - cursorValueY;

        const distanceBetweenCursorAndElement = Math.floor(Math.sqrt(distanceX ** 2 + distanceY ** 2));
        const shadowOpacity = defaultOpacity - growthRate(distance, distanceBetweenCursorAndElement * 0.001);
        const shadowSize = growthRate(defaultSize, distanceBetweenCursorAndElement * 0.01);
        const shadowBlur = growthRate(defaultBlur, distanceBetweenCursorAndElement * 0.01);
        const shadowDirectionX = distanceX * 0.08;
        const shadowDirectionY = distanceY * 0.08;
        const htmlElement = element as HTMLElement;

        const shadowOne = `rgb(${colorRgb}, ${shadowOpacity}) ${shadowDirectionX}px ${shadowDirectionY}px ${
          shadowBlur || 0
        }px ${shadowSize}px`;

        const shadowTwo = `rgb(${colorRgb}, ${shadowOpacity}) ${shadowDirectionX * 1.5}px ${shadowDirectionY * 1.5}px ${
          shadowBlur || 0
        }px ${shadowSize * 1.5}px`;

        htmlElement.style.setProperty("box-shadow", `${shadowOne} , ${shadowTwo}`);
      });
    });
    return () => {
      document.removeEventListener("mousemove", () => {});
    };
  }, []);

  return {
    color: colorRgb,
    applied: {
      "data-shadow": "true"
    }
  };
}

export default useShadow;
