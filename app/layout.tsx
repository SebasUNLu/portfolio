import Header from "../components/Header/Header";
import "./globals.css";
import Footer from "components/Footer/Footer";

export const metadata = {
  title: "Sebastián Pedro Marchetti",
  description: "Fullstack developer"
};

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang="en">
      <body>
        <Header />
        {children}
        <Footer />
      </body>
    </html>
  );
}
