"use client";

import Projects from "components/Projects/Projects";
import Skills from "components/Skills/Skills";
import Experience from "components/Experience/Experience";
import Contact from "components/Contact/Contact";
import Home from "components/Home/Home";
import Section from "components/Section/Section";
import AboutMe from "components/AboutMe/AboutMe";

export default function Portfolio() {
  const listSection = [
    { classname: "", title: "home", component: <Home /> },
    { classname: "", title: "about", component: <AboutMe /> },
    { classname: "", title: "experience", component: <Experience /> },
    { classname: "", title: "skills", component: <Skills /> },
    { classname: "", title: "projects", component: <Projects /> },
    { classname: "", title: "contact", component: <Contact /> }
  ];

  return (
    <main className="flex flex-col grow items-center justify-center self-center w-full mt-[var(--margin-content-top)]">
      {listSection.map((section) => {
        const { title, component, classname } = section;
        return <Section key={title} title={title} children={component} className={classname} />;
      })}
    </main>
  );
}
